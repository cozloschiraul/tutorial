var express = require('express');
var router = express.Router();

require('../database/connection')

let Things = require('../database/models/things')

let error = err => {
    console.log('err', err)
    res.status(400);
    res.end("eroare")
}

/* GET users listing. */
router.get('/', function(req, res, next) {
    Things.find({}).then(things => {
        res.json(things)
    }).catch(err =>{ 
        return error(err)
    })
});

router.get('/:THING_ID', function(req, res, next) {
    Things.findById(req.params.THING_ID).then(things => {
        res.json(things)
    }).catch(err =>{ 
        return error(err)
    })
});

router.post('/', function(req, res, next) {
    Things.create({
        name: req.body.name,
        createDate: new Date(),
        comment: req.body.comment
    }).then(() => {
        res.end("ok mama")
    }).catch(err => {
        return error(err)
    })
});

router.delete('/:THING_ID', function(req, res, next) {
    Things.findByIdAndDelete(req.params.THING_ID).then(() => {
        res.end("ok mama")
    }).catch(err => {
        return error(err)
    })
});

router.put('/:THING_ID', function(req, res, next) {
    Things.findByIdAndUpdate(req.params.THING_ID, {
        $set: req.body
    }).then(() => {
        res.end("ok mama")
    }).catch(err => {
        return error(err)
    })
});

module.exports = router;