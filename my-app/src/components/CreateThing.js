import React from 'react'
import ReactDOM from 'react-dom'


class CreateThing extends React.Component {

    state = {
        name: '',
        comment: ''
    }

    onChange = event => {
        this.setState({
          [event.target.name]: event.target.value
        })
    }

    sendThing = () => {
        this.props.onCreate({
            name: this.state.name,
            comment: this.state.comment
        })
    }

    render() {
        return <div>
           <input name='name' type='text' onChange={this.onChange} value={this.state.name} placeholder='Thing Name' />
           <input name='comment' type='text' onChange={this.onChange} value={this.state.comment} placeholder='Comment' />
           <button onClick={this.sendThing}>Send</button>
        </div>
    }
}

export default CreateThing