const mongoose = require('mongoose')

const Things = mongoose.model('Things', { 
    name: String,
    createDate: Date,
    comment: String
});

module.exports = Things