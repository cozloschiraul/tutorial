import React from 'react'
import ReactDOM from 'react-dom'


class ThingsList extends React.Component {
    render() {
        return <div>
            <h1>Lista mea:</h1>
            <ul>
                {this.props.list.length > 0 ? this.props.list.map(thing =>
                    <li>
                        {thing.name}
                        <button onClick={() => this.props.onDelete(thing._id)}> X </button>
                        <button onClick={() => this.props.onUpdate(thing._id)}> Update </button>
                    </li>) :
                    'nu'}
            </ul>
        </div>
    }
}

export default ThingsList