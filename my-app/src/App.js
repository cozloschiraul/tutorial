import React from 'react'
import ReactDOM from 'react-dom'
import ThingsList from './components/ThingsList'
import CreateThing from './components/CreateThing'
import axios from './utils/axios'

class ToDoContainer extends React.Component {

    state = {
        things: []
    }

    getThings = () => {
        axios.get('/things').then(response => {
            this.setState({ things: response.data })
        }).catch(err => {
            console.log(err)
            alert("Eroare")
        })
    }

    deleteThing = thingId => {
        if (window.confirm("Are uuu sureeE?")) {
            axios.delete('/things/' + thingId).then(() => {
                this.getThings()
            }).catch(err => {
                console.log(err)
                alert("Eroare")
            })
        }
    }

    updateThing = thingId => {
        let nume = window.prompt("Nume")
        if (nume) {
            axios.put('/things/' + thingId, { name: nume }).then(() => {
                this.getThings()
            }).catch(err => {
                console.log(err)
                alert("Eroare")
            })
        }
    }

    onCreateThing = thing => {
        axios.post('/things', thing).then(() => {
            this.getThings()
        }).catch(err => {
            console.log(err)
            alert("Eroare")
        })
    }

    componentDidMount() {
        this.getThings()
    }

    render() {
        return <div class='container'>
            <CreateThing onCreate={this.onCreateThing} />
            <ThingsList list={this.state.things} onDelete={this.deleteThing} onUpdate={this.updateThing} />
        </div>
    }
}

export default ToDoContainer